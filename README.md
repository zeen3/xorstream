# xorstream

Utility for transforming an input stream to an output stream with a bytewise xor transform applied.

As an installed executable, takes a vector of 4, 5, or 6 bit wide (hex, 32, 64) which then pipes from stdin to stdout.

As a dependency it exposes a `Transformer` and a `XorIter` or `XorStream`. `Transformer` takes a vector of bytes and an `impl Read`. The `Transformer` implements `IntoIter` or `IntoStream` depending on `features = [ "std" ]` or `features = [ "stream" ]`.

Installing with `x86_64-unknown-linux-musl` (for example) can increase the rate it occurs greatly.

```shell
$ cargo install xorstream --features bin --target x86_64-unknown-linux-musl
```

Example:

```shell
$ yes 0 | cargo run --features bin --release --target x86_64-unknown-linux-musl -- -4 714b
```

This pipes `AAAAAAAAAAAAAAAAAAAAA` infinitely to stdout, using std xorstream.


