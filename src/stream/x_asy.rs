use async_std::{
	io::{self, Read, ErrorKind},
	task::{Context, Poll},
	stream::{
		IntoStream, Stream
	}
};
// use futures_io::AsyncRead;
use std::pin::Pin;
use ::core::marker::Unpin;
#[derive(Debug)]
pub struct Transformer<S: Read + Unpin> {
	index: usize,
	xor: Vec<u8>,
	source: S
}
impl<S: Read + Unpin> Unpin for Transformer<S> {}
impl<S: Read + Unpin> Transformer<S> {
	pub fn new(xor: Vec<u8>, source: S) -> Self {
		Transformer { index: 0, xor, source }
	}
	fn next_xor_byte(&mut self) -> u8 {
		let xored = self.xor[self.index];
		self.index += 1;
		if self.index == self.xor.len() {
			self.index = 0;
		}
		xored
	}
}
impl<S: Read + Unpin> Read for Transformer<S> {
	fn poll_read(mut self: Pin<&mut Self>, cx: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<usize>> {
		match Pin::new(&mut (*self).source).poll_read(cx, buf) {
			Poll::Ready(Ok(res)) => {
				for i in 0..res {
					buf[i] ^= self.next_xor_byte();
				}
				Poll::Ready(Ok(res))
			},
			or => or
		}
	}
}
impl<S: Read + Unpin> IntoStream for Transformer<S> {
	type IntoStream = XorStream<S>;
	type Item = io::Result<Vec<u8>>;
	fn into_stream(self) -> Self::IntoStream {
		XorStream {
			transformer: self
		}
	}
}
#[derive(Debug)]
pub struct XorStream<S: Read + Unpin> {
	transformer: Transformer<S>,
}
impl<S: Read + Unpin> Unpin for XorStream<S> {}
impl<S: Read + Unpin> Stream for XorStream<S> {
	type Item = io::Result<Vec<u8>>;
	fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
		let mut buf = [0u8; 1024];
		loop {
			match Pin::new(&mut (*self).transformer).poll_read(cx, &mut buf) {
				Poll::Pending => return Poll::Pending,
				Poll::Ready(Ok(0)) => return Poll::Ready(None),
				Poll::Ready(Ok(n)) => return Poll::Ready(Some(Ok(buf[..n].to_vec()))),
				Poll::Ready(Err(e)) => match e.kind() {
					ErrorKind::Interrupted => continue,
					_ => return Poll::Ready(Some(Err(e))),
				}
			}
		}
	}
}
#[cfg(test)]
mod test {
	use super::*;
	use async_std::{io::{Result, repeat}, stream::StreamExt};
	type R = Result<()>;
	#[async_std::test]
	async fn test() -> R {
		let trans = Transformer::new([0, 1, 2, 3].to_vec(), repeat(127));
		let mut _iter = trans.into_stream();
		while let Some(v) = _iter.next().await {
			assert!(v?.chunks(4).all(|chunk| chunk == &[127, 126, 125, 124]));
			break;
		}
		Ok(())
	}
}
