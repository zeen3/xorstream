use std::{fmt, io::{BufReader, Write, BufWriter}};
include!("../stream/x_std.rs");
#[derive(structopt::StructOpt, Debug)]
#[structopt(name = "xorstream", about = "streams data from stdin to stdout transforming with a given key of base64/32/16")]
/// Must use only one input option
struct Args {
	/// RFC4648; A..=Z a..=z 0..=9 +/
	#[structopt(short = "6", long)]
	base64: Option<String>,
	/// RFC4648; A..=Z 2..=7
	#[structopt(short = "5", long)]
	base32: Option<String>,
	/// RFC4648; 0..=9 A..=F
	#[structopt(short = "4", long)]
	hex: Option<String>,
}
#[derive(Debug)]
enum DecodeError {
	Hex(hex::FromHexError),
	Base32Error,
	Base64(base64::DecodeError),
	NoInputs,
	TooManyInputs
}
impl fmt::Display for DecodeError {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
		fmt::Debug::fmt(self, fmt)
	}
}
impl ::std::error::Error for DecodeError {}
impl Args {
	fn to_bin(mut self) -> Result<Vec<u8>, DecodeError> {
		use DecodeError::*;
		match (self.base64.take(), self.base32.take(), self.hex.take()) {
			(Some(s), None, None) => match base64::decode(&s) {
				Err(e) => Err(Base64(e)),
				Ok(ok) => Ok(ok)
			},
			(None, Some(s), None) => {
				match base32::decode(base32::Alphabet::RFC4648 { padding: true }, &s) {
					Some(ok) => Ok(ok),
					None => Err(Base32Error)
				}
			},
			(None, None, Some(s)) => match hex::decode(&s) {
				Err(e) => Err(Hex(e)),
				Ok(ok) => Ok(ok)
			},
			(None, None, None) => Err(NoInputs),
			_ => Err(TooManyInputs)
		}
	}
}

#[paw::main]
fn main(args: Args) -> Result<(), Box<dyn ::std::error::Error>> {
	let data = args.to_bin()?;
	let stdin = io::stdin();
	let stdout = io::stdout();
	let reader = BufReader::new(stdin.lock());
	let transform = Transformer::new(data, reader);
	let mut writer = BufWriter::new(stdout.lock());
	for chunk in transform {
		writer.write_all(&chunk?)?;
	}
	writer.flush()?;
	Ok(())
}
