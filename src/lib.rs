//! # xorstream
//!
//! Utility crate to xor readable data with more readable data.
//!
//! In the command line version, it takes a xor property (hex, base32, base64),
//! takes input from stdin and streams to stdout. Only possible errors are those
//! of an argument, input, and output errors.
#[cfg(not(feature = "async-std"))]
mod stream {
	mod x_std;
	pub use x_std::*;
}
#[cfg(feature = "async-std")]
mod stream {
	mod x_asy;
	pub use x_asy::*;
}
pub use stream::*;
